import Vue from 'vue'
import App from './App.vue'
import NProgress from 'vue-nprogress'
import router from './router'
const HelloJs = require('hellojs/dist/hello.all.min.js');
const VueHello = require('vue-hellojs');
import VueParticles from 'vue-particles'
import VueTextareaAutosize from 'vue-textarea-autosize'
import Axios from 'axios';

const TWINT_API = (process.env.NODE_ENV === 'production') ? 'https://twint.duckdns.org' : 'http://127.0.0.1:8081';
const AGREGADOR_API = (process.env.NODE_ENV === 'production') ? 'https://agregador-api.herokuapp.com' : 'http://agregador-api.test'
Axios.defaults.baseURL = AGREGADOR_API + '/api';
Axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

HelloJs.init({
  twitter: 'bSy3gbq0eGIyqdKAQxdGrGGGv'
}, { redirect_uri: '../redirect.html' })
 
Vue.use(VueTextareaAutosize)
Vue.use(VueHello, HelloJs);
Vue.use(VueParticles)
Vue.use(NProgress)
const nprogress = new NProgress()

const $auth = new Vue({
  data() {
    return {
      meta: localStorage.getItem("user") || null
    };
  },
  computed: {
    user() {
      return JSON.parse(this.meta)
    }
  },
  methods: {
    login(user) {
      this.meta = JSON.stringify(user)
      localStorage.setItem("user", this.meta)
    },
    logout() {
      localStorage.removeItem("user")
      this.meta = null
    }
  }
})

Vue.mixin({
  created(){
    this.$http.defaults.headers.common['Authorization'] = "Bearer " + this.apiToken || ""
  },
  computed: {
    apiToken() {
      return (this.$root.$options.$auth && 
              this.loggedUser) ? this.$root.$options.$auth.user.api_token : null
    },
    loggedUser(){
      return this.$root.$options.$auth.user
    }
  }
})

Vue.prototype.$http = Axios;
Vue.config.productionTip = false
 

/* eslint-disable no-new */
new Vue({
    nprogress,
    $auth,
    TWINT_API,
    router: router, 
    render: h => h(App),
}).$mount('#app')
