import Vue from 'vue'
import Router from 'vue-router'
import LoginPage from '@/components/LoginPage'
import DashboardPage from '@/components/DashboardPage'
import ExplorePage from '@/components/ExplorePage'
import SettingsPage from '@/components/SettingsPage'
import TweetPage from '@/components/TweetPage'
import MentionsPage from '@/components/MentionsPage'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'login',
      component: LoginPage,
      meta: { 
        guest: true
      }
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: DashboardPage,
      // props: { default: true }
      meta: { 
        requiresAuth: true
      }
    },
    {
      path: '/explore',
      name: 'explore',
      component: ExplorePage,
      meta: { 
        requiresAuth: true
      }
    },
    {
      path: '/settings',
      name: 'settings',
      component: SettingsPage,
      meta: { 
        requiresAuth: true
      }
    },
    {
      path: '/tweet',
      name: 'tweet',
      component: TweetPage,
      meta: { 
        requiresAuth: true
      }
    },
    {
      path: '/mentions',
      name: 'mentions',
      component: MentionsPage,
      props: {
        default: true,
        type: 'mentions'
      },
      meta: { 
        requiresAuth: true
      }
    },
    // {
    //   path: '/admin',
    //   name: 'admin',
    //   component: Admin,
    //   meta: { 
    //     requiresAuth: true,
    //   }
    // },
  ]
})


router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if ( ! localStorage.getItem('user')) {
      return next({
        path: '/',
        params: { nextUrl: to.fullPath }
      })
    }
    return next()
  } else if(to.matched.some(record => record.meta.guest)) {
    if( ! localStorage.getItem('user')){
      return next()
    }
    return next({ name: 'dashboard'})
  }
  return next()
})


export default router