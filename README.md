# my-tweets


## How to use

```
yarn install
# or
npm install
```

### Start local development

To start development run:

```
yarn dev
```

This will compiles and start hot-reloads server on your localhost [http://127.0.0.1:8080/](http://127.0.0.1:8080/).

## Live Demo

Branch master are auto deployed [my-tweets live](http://my-tweets.netlify.com).

[![Netlify Status](https://api.netlify.com/api/v1/badges/7cb40c1e-d9fc-4507-9a7e-06fa5cecac05/deploy-status)](https://app.netlify.com/sites/my-tweets/deploys)

## Tools

- [vue-chartjs](https://vue-chartjs.org/guide/)
- [template](off/admin-template.html)

### Twitter

- [twitter developer apps](https://developer.twitter.com/en/apps)
- [twitter proxy auth server](https://auth-server.herokuapp.com/#signin)
- [guide login with twitter](https://developer.twitter.com/en/docs/twitter-for-websites/log-in-with-twitter/guides/implementing-sign-in-with-twitter)

http://adodson.com/hello.js/src/modules/twitter.js

https://www.websequencediagrams.com/
https://cngu.github.io/vue-typer/
https://github.com/netlify/cli/issues/158
